package Servlets;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Controllers.UsersControl;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author kelvi
 */
@WebServlet(urlPatterns = {"/users"}, initParams = {
    @WebInitParam(name = "_id", value = "Valor")
    , @WebInitParam(name = "Primeiro_Nome", value = "Valor")
    , @WebInitParam(name = "Ultimo_Nome", value = "Valor")})
public class UsersServlet extends HttpServlet {

    
    protected void getUsers(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        
        try (PrintWriter out = response.getWriter()) {    
            UsersControl usersControl = new UsersControl();
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            
            if (username.length() > 0 && password.length() > 0){
                ResultSet rs = usersControl.getUser(username, password);

                String json = "{";
                while (rs.next()) {
                    json += "\"_id\": \""+rs.getString("_id")+"\",";
                    json += "\"username\": \""+rs.getString("username")+"\",";
                    json += "\"password\": \""+rs.getString("password")+"\",";
                    json += "\"first_name\": \""+rs.getString("first_name")+"\",";
                    json += "\"last_name\": \""+rs.getString("last_name")+"\",";
                    json += "\"profile_picture\": \""+rs.getString("profile_picture")+"\"";
                }
                json += "}";

                out.println(json);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
    protected void setUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            UsersControl usersControl = new UsersControl();
            
            String username = request.getParameter("username");
            
            boolean success = usersControl.inserir(username);
            
            out.println("/setUser");
            
            if (success){
                out.println("Usuário inserido com sucesso");
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        getUsers(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        setUser(request, response);
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
