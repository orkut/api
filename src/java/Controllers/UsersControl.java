/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Banco.BancoModels;
import Models.User;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 *
 * @author kelvi
 */
public class UsersControl {
    
    private User user;
    private BancoModels bmConsulta = null;
    
    public UsersControl() {
        user = new User();
    }
    
    public ResultSet getUser (String username, String password){
        String sql = "SELECT * FROM USERS WHERE USERNAME = '" + username + "' AND PASSWORD = '" + password + "'";
        return this.consultar(sql);
    }
    
    public boolean inserir(String username) {
        this.user.setUsername(username);
        boolean log = this.operarBancoDados(1);
        return log;
    }
    
    public ResultSet consultar(String sql) {
        bmConsulta = new BancoModels();
        boolean log = bmConsulta.conectar();
        ResultSet rs = null;
        if (log) //nao houve problemas na conexao
             rs = bmConsulta.consultar(sql);
        return rs;
    }
    
    private boolean operarBancoDados(int op) {
        String sql = "";
        
        if (op == 1) //operação de inserir
            sql = "INSERT INTO users (`username`, `password`, `first_name`, `last_name`, `profile_picture`) " + 
                  "VALUES " + 
                  "('"+this.user.getUsername()+"','','','','');";

        BancoModels b = new BancoModels();
        boolean log = b.conectar();
        if (log) //nao houve problemas na conexao
            log = b.comandoStatment(sql);
        b.desconectar();
        return log;
    }
    
    public void fecharConexao() {
        if (this.bmConsulta != null)
            bmConsulta.desconectar();
    }
}
